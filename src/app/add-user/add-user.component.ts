import { Component, OnInit } from '@angular/core';

import { APIServiceService } from '../services/apiservice.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  constructor(private api:APIServiceService) { }
  newbuff:number=null;
  newid:number=null;
  newname: string="";
  newemail: string="";
  newaddress: string="";
  newnumber: string="";
  newcompany: string="";

  ngOnInit() {
    
    
  }
 
  // newid:number=null;
  // newname: string="";
  // newemail: string="";
  // newaddress: string="";
  // newnumber: string="";
  // newcompany: string="";

  addUser()
  {
    this.api.UserList.push({ "id":this.newid, "name": this.newname, "email": this.newemail, "address.street": this.newaddress, "phone": this.newnumber, "company": this.newcompany})

    this.newname= "";
    this.newemail= "";
    this.newaddress= "";
    this.newnumber= "";
    this.newcompany= "";
  }
}
