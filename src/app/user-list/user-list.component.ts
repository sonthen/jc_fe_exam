import { Component, OnInit } from '@angular/core';

import {APIServiceService} from '../services/apiservice.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  constructor(private api:APIServiceService) { }
  // UserList:Object[]= this.api.UserList;

  ngOnInit() {
    // this.api.getData()
    // .subscribe(result => this.UserList = result);
  }

  deleteData (index){
    this.api.UserList.splice(index, 1);
  }

}
